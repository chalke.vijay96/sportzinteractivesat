//
//  SportzInteractiveSATApp.swift
//  SportzInteractiveSAT
//
//  Created by JM MAC MINI 4 on 10/01/23.
//

import SwiftUI

@main
struct SportzInteractiveSATApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
//            ContentView()
//                .environment(\.managedObjectContext, persistenceController.container.viewContext)
            MatchDashBoardView().environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
