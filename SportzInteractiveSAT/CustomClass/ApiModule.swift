//
//  ApiModule.swift
//  SportzInteractiveSAT
//
//  Created by JM MAC MINI 4 on 10/01/23.
//

import Foundation

class ApiModule: NSObject {
    
    class func getApiDataInDict<T:Decodable>(methodName :String?,headerFieldVal :String?,pathParam :String?,jsonParam:Dictionary<String,Any>?, headers:Dictionary<String, Any>?,requestURL:String,returnInCaseOffailure:Bool,decodingType: T.Type,completion : @escaping(_ status:Bool,_ model:T?) -> Void)
    {
        LoadingIndicatorView.show()
        var request = URLRequest(url: URL(string: requestURL)!)
        request.httpMethod = methodName!
        request.addValue(headerFieldVal!, forHTTPHeaderField: "Content-Type")
        
        let session = URLSession.shared
        let task = session.dataTask(with: request, completionHandler: { data, response, error -> Void in
            guard let data = data else { return }
            do{
                let model = try JSONDecoder().decode(T.self, from: data)
                completion(true, model)
            }catch{
                print("CATCH")
                completion(false, nil)
            }
            DispatchQueue.main.async {
                LoadingIndicatorView.hide()
            }
        })
        
        task.resume()
    }
    
}

class Utils: NSObject {
    
    class func getDateFromStr(dateStr : String, formatterStr : String) -> Date? {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formatterStr
        let date = dateFormatter.date(from: dateStr)
        return date
        
    }
    
}

