//
//  APIConstant.swift
//  SportzInteractiveSAT
//
//  Created by JM MAC MINI 4 on 10/01/23.
//

import Foundation
import UIKit

var kWindow = UIWindow(frame: UIScreen.main.bounds)
var kScreenSize = UIScreen.main.bounds.size
var kKeyWindows = UIApplication.shared.keyWindow
let kFrameWidth = UIScreen.main.bounds.size.width
let kFrameHeight = UIScreen.main.bounds.size.height

struct APIUrl{
    static let Base_URL = "https://demo.sportz.io/"
    static let NZ_IN = "nzin01312019187360.json"
    static let SA_PK = "sapk01222019186652.json"
}

struct AlertMessage{
    static let CHECK_NET_CONNECTION = "Please check your internet connection"
    static let NO_DATA = "No data found !"
    
}

struct UserDefaultConstant{
    static let savedMatchModel = "Saved_MatchModel"
    
}



public enum HeaderKey:String {
    case ContentType = "Content-Type"
    case aAccept = "Accept"

}

public enum HeaderValue:String {
    case Json = "application/json"
    case Multiform = "multipart/form-data"
    case urlencode = "application/x-www-form-urlencoded"

}


public enum MethodType:String {
    case GET = "GET"
    case POST = "POST"
    case DELETE = "DELETE"
    case PUT = "PUT"

}
