//
//  MatchDetailTableViewCell.swift
//  SportzInteractiveSAT
//
//  Created by JM MAC MINI 4 on 11/01/23.
//

import UIKit

class MatchDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var btn_Proceed: UIButton!
    @IBOutlet weak var lbl_Date: UILabel!
    @IBOutlet weak var lbl_Venue: UILabel!
    @IBOutlet weak var lbl_Match: UILabel!
    @IBOutlet weak var lbl_team: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
