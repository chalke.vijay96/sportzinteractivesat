//
//  PlayerInfoTableViewCell.swift
//  SportzInteractiveSAT
//
//  Created by JM MAC MINI 4 on 11/01/23.
//

import UIKit

class PlayerInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var view_Captain: DesignableView!
    @IBOutlet weak var lbl_Captain: UILabel!
    @IBOutlet weak var lbl_PlayerName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
