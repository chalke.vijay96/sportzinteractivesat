//
//  PlayerDashboard.swift
//  SportzInteractiveSAT
//
//  Created by JM MAC MINI 4 on 11/01/23.
//

import UIKit

class PlayerDashboard: UIViewController {

    @IBOutlet weak var segmented_Control: UISegmentedControl!
    var matchDetailVM = MatchDetailViewModel()
    var teamNameArr : [String] = []
    var playerInfoArr : [Player]?
    @IBOutlet weak var tble_Views: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tble_Views?.register(UINib(nibName: "PlayerInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "PlayerInfoTableViewCell")
        
        teamNameArr = matchDetailVM.getTeamNameArray()
        teamNameArr.append("All")
        print(teamNameArr)
        for i in 0..<teamNameArr.count {
            segmented_Control.setTitle("\(teamNameArr[i])", forSegmentAt: i)
        }
        segmented_Control.selectedSegmentIndex = teamNameArr.count - 1
        playerInfoArr = matchDetailVM.getPlayerNameArray(teamNameStr: teamNameArr[segmented_Control.selectedSegmentIndex])
        print(playerInfoArr)
        self.tble_Views.reloadData()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func btnBackClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func segment_Clicked(_ sender: Any) {
        
        if (teamNameArr.count > 0){
            let selectedTeam = teamNameArr[segmented_Control.selectedSegmentIndex]
            playerInfoArr = matchDetailVM.getPlayerNameArray(teamNameStr: selectedTeam)
            self.tble_Views.reloadData()
        }
    }
    
    func showALert(title:String, msg:String){
        let ac = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let submitAction = UIAlertAction(title: "Ok", style: .default) { action in
        }
        ac.addAction(submitAction)
        present(ac, animated: true)
    }
}


extension PlayerDashboard : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playerInfoArr?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlayerInfoTableViewCell", for: indexPath) as! PlayerInfoTableViewCell
        if let playerInfo = playerInfoArr?[indexPath.row]{
            cell.lbl_PlayerName.text = playerInfo.nameFull
            //            cell.view_Captain.isHidden = !(playerInfo.iscaptain ?? false)
            cell.view_Captain.isHidden = true
            if let isCaptain = playerInfo.iscaptain {
                cell.lbl_Captain.text = "*C"
                cell.view_Captain.isHidden = !isCaptain
            }
            if let iskeeper = playerInfo.iskeeper {
                cell.lbl_Captain.text = "*WC"
                cell.view_Captain.isHidden = !iskeeper
            }
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let playerInfo = playerInfoArr?[indexPath.row]{
            var msgStr = ""
            if let battInfo = playerInfo.batting, let style = battInfo.style, let runs = battInfo.runs{
                msgStr = "Batting Style :- \(style) , Runs :- \(runs)"
            }
            if let bowlInfo = playerInfo.bowling, let style = bowlInfo.style, let wicket = bowlInfo.wickets{
                msgStr = msgStr + "\n" + "Bowling Style :- \(style) , Wickets :- \(wicket)"
            }
            self.showALert(title: playerInfo.nameFull ?? "Player Info", msg: msgStr)
        }
    }
}
