//
//  MatchDashboard.swift
//  SportzInteractiveSAT
//
//  Created by JM MAC MINI 4 on 10/01/23.
//

import UIKit

import Foundation
import SwiftUI


struct MatchDashBoardView: View {
    let reference = Reference<WelcomeScreenVC>()
    
    var body: some View {
        DashBoardView(reference: reference)
    }
}

struct DashBoardView: UIViewControllerRepresentable {
    let reference: Reference<WelcomeScreenVC>
    
    func makeUIViewController(context: Context) -> WelcomeScreenVC {
        let controller = WelcomeScreenVC()
        
        // Set controller to the reference
        reference.object = controller

        return controller
    }
    
    func updateUIViewController(_ viewController: WelcomeScreenVC, context: Context) {
    }
}

class Reference<T: AnyObject> {
    weak var object: T?
}

class WelcomeScreenVC: UIViewController {
    
    override func viewDidLoad() {
        self.view.backgroundColor = .lightGray
        let buttonOne:UIButton = UIButton(frame: CGRect(x: 70, y: 100, width: self.view.frame.width-140, height: 55))
        buttonOne.tag = 1
        buttonOne.backgroundColor = .blue
        buttonOne.layer.cornerRadius = 10.0
        buttonOne.setTitle("New Zealand Vs India", for: .normal)
        buttonOne.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        self.view.addSubview(buttonOne)
        
        let buttonTwo:UIButton = UIButton(frame: CGRect(x: 70, y: 200, width: self.view.frame.width-140, height: 55))
        buttonTwo.tag = 2
        buttonTwo.backgroundColor = .blue
        buttonTwo.layer.cornerRadius = 10.0
        buttonTwo.setTitle("South Africa Vs Pakistan", for: .normal)
        buttonTwo.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
        self.view.addSubview(buttonTwo)
        
    }

    @objc func buttonClicked(sender : UIButton) {
         print("buttonOneClicked")
        let vc = MatchDashboard(nibName: "MatchDashboard", bundle: nil)
        vc.modalPresentationStyle = .overFullScreen
        vc.matchTag = sender.tag
        self.present(vc, animated: true, completion: nil)
        
    }
    
    
}

class MatchDashboard: UIViewController {

    @IBOutlet weak var tblVIew: UITableView!
    var matchTag = 0
    var matchDetailVM = MatchDetailViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tblVIew?.register(UINib(nibName: "MatchDetailTableViewCell", bundle: nil), forCellReuseIdentifier: "MatchDetailTableViewCell")
        matchDetailVM.delegate = self
        matchDetailVM.callMatchDetailAPI(tag: matchTag)
        
    }
    
    @IBAction func btnBackClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension MatchDashboard : MatchDetailViewModelDelegate{
    
    func reloadMatchData() {
        self.tblVIew.reloadData()
    }
    
}

extension MatchDashboard : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MatchDetailTableViewCell", for: indexPath) as! MatchDetailTableViewCell
        cell.lbl_team.text = matchDetailVM.teamName
        if let matchInfo = matchDetailVM.mainMatchDetailModel?.matchdetail?.series{
            cell.lbl_Match.text = matchInfo.name
        }
        if let matchVenue = matchDetailVM.mainMatchDetailModel?.matchdetail?.venue{
            cell.lbl_Venue.text = "Venue :- \(matchVenue.name ?? "")"
        }
        if let matchDate = matchDetailVM.mainMatchDetailModel?.matchdetail?.match, let dateStr = matchDate.date, let timeStr = matchDate.time{
        
            let dateValue = Utils.getDateFromStr(dateStr: "\(dateStr) \(timeStr)", formatterStr: "MM/dd/yyyy HH:mm") ?? Date()
            let formatDate = dateValue.getFormattedDate(format: "dd MMM yyyy hh:mm a")
            cell.lbl_Date.text = formatDate
        }
        cell.btn_Proceed.addTarget(self, action: #selector(btnProceedClicked(sender:)), for: .touchUpInside)

        return cell
        
    }
    
    @objc func btnProceedClicked(sender: UIButton){
        let vc = PlayerDashboard(nibName: "PlayerDashboard", bundle: nil)
        vc.modalPresentationStyle = .overFullScreen
        vc.matchDetailVM = matchDetailVM
        self.present(vc, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 180
    }

}
