//
//  MatchDetailViewModel.swift
//  SportzInteractiveSAT
//
//  Created by JM MAC MINI 4 on 10/01/23.
//

import Foundation

protocol MatchDetailViewModelDelegate {
    func reloadMatchData()
}

class MatchDetailViewModel {
    
    var mainMatchDetailModel : MatchDetailModel?
    var delegate : MatchDetailViewModelDelegate!
    var teamName = ""
    
    func callMatchDetailAPI(tag : Int){
        let urlStr = (tag == 1) ? APIUrl.NZ_IN : APIUrl.SA_PK
        ApiModule.getApiDataInDict(methodName: MethodType.GET.rawValue, headerFieldVal: HeaderValue.Json.rawValue, pathParam: nil, jsonParam: nil, headers: nil, requestURL: APIUrl.Base_URL + urlStr, returnInCaseOffailure: true, decodingType: MatchDetailModel.self) { (flag, result) in
            if let model : MatchDetailModel = result{
                self.mainMatchDetailModel = model
                DispatchQueue.main.async {
                    self.setOtherInfo()
                }
            }
        }
    }
    
    func setOtherInfo(){
        var teamNameArr : [String] = []
        if let teamArr = self.mainMatchDetailModel?.teams{
            teamName = ""
            for teamVal in teamArr {
                let values : Team = teamVal.value
                if let nameShort = values.nameShort {
                    teamNameArr.append(nameShort)
                }
            }
        }
        teamName = teamNameArr.joined(separator:" Vs ")
        print("teamName \(teamName)")
        self.delegate.reloadMatchData()
    }
    
    func getTeamNameArray() -> [String]{
        var teamNameArr : [String] = []
        if let teamArr = self.mainMatchDetailModel?.teams{
            for teamVal in teamArr {
                let values : Team = teamVal.value
                if let nameShort = values.nameFull {
                    teamNameArr.append(nameShort)
                }
            }
        }
        return teamNameArr
    }
    
    func getPlayerNameArray(teamNameStr : String) -> [Player]?{
        
        var playersArr: [Player] = []
        if let teamArr = self.mainMatchDetailModel?.teams{
            for teamVal in teamArr {
                let values : Team = teamVal.value
                if let teamFullName = values.nameFull {
                    if (teamNameStr.lowercased() == teamFullName.lowercased()){
                        if let playerModelArr : [String: Player] = values.players {
                            for playerVal in playerModelArr {
                                let playerModel : Player = playerVal.value
                                playersArr.append(playerModel)
                            }
                        }
                        
                    }else if (teamNameStr.lowercased() == "all"){
                        if let playerModelArr : [String: Player] = values.players {
                            for playerVal in playerModelArr {
                                let playerModel : Player = playerVal.value
                                playersArr.append(playerModel)
                            }
                        }
                    }
                }
            }
        }
        print(playersArr.count)
        return playersArr
    }
}
